#!/bin/bash

# SPDX-FileCopyrightText: 2022 eclipse foundation
# SPDX-License-Identifier: EPL-2.0

# Bash strict-mode
set -o errexit
set -o nounset
set -o pipefail

environment="-${1:-}"
[[ "$environment" == "-" ]] && echo "Please provide environment as first argument" && exit 1
[[ "$environment" == "-prod" ]] && environment=""

kubectx

(trap 'trap - SIGTERM && kill 0' SIGINT SIGTERM EXIT; \
  kubectl port-forward deployment/synapse-admin 8080:8080 -n "chat-matrix${environment}" & \
  kubectl port-forward deployment/synapse 8008:8008 -n "chat-matrix${environment}" & \
  kubectl port-forward deployment/matrix-media-repo 8000:8000 -n "chat-matrix${environment}"
)
