#!/bin/bash

# SPDX-FileCopyrightText: 2022 eclipse foundation
# SPDX-License-Identifier: EPL-2.0

# Bash strict-mode
set -o errexit
set -o nounset
set -o pipefail

environment="-${1:-}"

[[ "$environment" == "-" ]] && echo "Please provide environment as first argument" && exit 1
[[ "$environment" == "-prod" ]] && environment=""
export ENVIRONMENT=${environment}
SSH_CONFIG_HOME_DIR="${2:-$HOME}"

SCRIPT_FOLDER="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"

(trap 'trap - SIGTERM && kill 0' SIGINT SIGTERM EXIT; \
  dnsmasq --no-daemon \
        --address=/matrix"${environment}".eclipse.org/127.0.0.1 \
        --address=/synapse-admin"${environment}".eclipse.org/127.0.0.1 \
        --address=/chat"${environment}".eclipse.org/127.0.0.1 \
        --address=/matrix-media-repo"${environment}".eclipse.org/127.0.0.1 \
        --address=/elementweb"${environment}".eclipse.org/127.0.0.1 \
        --address=/.okd-c1.eclipse.org/127.0.0.1 \
        --address=/.okd-c2.eclipse.org/127.0.0.1 & \
  ssh -T \
    -F "${SSH_CONFIG_HOME_DIR}/.ssh/config" \
    -L 1443:api.okd-c1.eclipse.org:443 \
    -L 16443:api.okd-c1.eclipse.org:6443 \
    -L 2443:api.okd-c2.eclipse.org:443 \
    -L 26443:api.okd-c2.eclipse.org:6443 \
    bastion > /dev/null &\
  caddy run --config "$SCRIPT_FOLDER/Caddyfile" 
)
