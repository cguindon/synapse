<!--
SPDX-FileCopyrightText: 2022 eclipse foundation
SPDX-License-Identifier: EPL-2.0
-->

# EF - synapse Matrix Server


# sources

https://schnerring.net/blog/deploy-a-matrix-homeserver-to-azure-kubernetes-service-aks-with-terraform/
https://github.com/ananace/matrix-synapse



# generate configuration

kubectl run matrix-generate-pod --namespace matrix \
  --stdin \
  --rm \
  --restart=Never \
  --command=true \
  --image matrixdotorg/synapse:latest \
  --env="SYNAPSE_REPORT_STATS=yes" \
  --env="SYNAPSE_SERVER_NAME=matrix.schnerring.net" \
  -- bash -c "/start.py generate && sleep 300"

  kubectl cp matrix-generate-pod:/data synapse-config --namespace matrix



  # register a new user

  kubectl get pods --namespace matrix
  kubectl exec --stdin --tty --namespace matrix matrix-deploy-xxxxxxxxxx-xxxxx -- bash

  register_new_matrix_user \
  --admin \
  --user michael \
  --config /data/homeserver.yaml \
  http://localhost:8008



  Synapse Admin UI requires access to the _synapse/admin endpoint. But we do not want to expose that endpoint to the public internet, so we have to connect to it by other means. kubectl port-forward allows us to securely forward a local port to a port on a Kubernetes service:
  kubectl port-forward service/matrix-svc --namespace matrix 8008:8008

  We can now enter http://localhost:8008 as homeserver URL and login to the admin UI with the user we created earlier:

# Becoming a admin

https://matrix-org.github.io/synapse/latest/usage/administration/admin_api/

```
SELECT * from users;
UPDATE users SET admin = 1 WHERE name = '@foo:bar.com';
```


# delegation 

https://github.com/matrix-org/synapse/blob/develop/docs/delegate.md

implementation
https://nonedhudla.xyz/installing-synapse-on-kubernetes/


# postgresql
https://github.com/matrix-org/synapse/blob/develop/docs/postgres.md


# contrib

https://github.com/matrix-org/synapse/tree/master/contrib

grafana, prometheus, ...

# CEPH integration

```yaml
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: fast
provisioner: kubernetes.io/rbd
parameters:
  monitors: 10.16.153.105:6789
  adminId: kube
  adminSecretName: ceph-secret
  adminSecretNamespace: kube-system
  pool: kube
  userId: kube
  userSecretName: ceph-secret-user
  userSecretNamespace: default
  fsType: ext4
  imageFormat: "2"
  imageFeatures: "layering"

```


# cmd

kubectl apply -f namespace.yaml

## postgres
kubectl apply -f ./postgresql/secret.yaml
kubectl apply -f ./postgresql/config.yaml
kubectl apply -f ./postgresql/persistent-volume.yaml
kubectl apply -f ./postgresql/deployment.yaml
kubectl apply -f ./postgresql/service.yaml


test : 

```
kubectl get pods
kubectl exec -it postgres-57f4746d96-7z5q8 -- psql -U postgres
```



# debug statefulset

kubectl  describe statefulset postgresql-statefulset --namespace matrix

# generate signing key

```
docker run -it  -e SYNAPSE_SERVER_NAME=matrix-dev.eclipse.org -e SYNAPSE_REPORT_STATS=no -v $PWD/config/gen:/data docker.io/eclipsecbi/synapse:latest generate
```

Get secrets: `registration_shared_secret`, `macaroon_secret_key`, `form_secret` from files `./config/gen/homeserver.yaml` 
And store the signing keys from file: `./config/gen/matrix-dev.eclipse.org.signing.key`

# test image locally

docker build . -t synapse

```
docker run -v $PWD/test:/synapse --env SYNAPSE_CONFIG_PATH=/synapse/config --env SYNAPSE_SERVER_NAME=matrix-staging.eclipse.org heurtemattes/synapse
```

# Synapse-admin

Admin API `/_synapse/admin` is not exposed, need to access directly via tunnel.

## Access via tunnel

Run commands in 2 terminals: 

```
sudo ./admin/tunnel.sh $HOME
```

```
./admin/admin.sh
```

# Administration

https://www.tedomum.net/dev/service/matrix/administration/#nettoyage-du-serveur



# Media-repo

https://quentin.dufour.io/blog/2021-09-14/matrix-synapse-s3-storage/


# Modules synapse-user-restrictions

https://github.com/matrix-org/synapse-user-restrictions

# Admin UI

## Accessing via port-forward

Caddyfile configuration

```
matrix-admin-staging.eclipse.org {
        tls internal
        reverse_proxy http://localhost:8080 {
                transport http {
                }
        }
}
```

dnsmasq:

```
address=/matrix-admin-staging.eclipse.org/127.0.0.1
address=/matrix-admin.eclipse.org/127.0.0.1
```

```shell
kubectl port-forward deployment/matrix-synapse-admin-deployment 8080:8080 -n chat-matrix-staging
```

Add exception in firefox configuration: Settings -> Certificates - View Certificates... -> tab 'Servers'; Click 'Add Exception...'
In popin: location: https://matrix-admin-staging.eclipse.org -> Click Get certificate -> Click 'Confirm Security Exception'.

Browser url: 
* staging: https://matrix-admin-staging.eclipse.org 
* prod: https://matrix-admin.eclipse.org


## appservice-slack

### Generating file

docker run -v $PWD/config/:/config/ matrixdotorg/matrix-appservice-slack -r -c /config/appservice-slack-config.yaml -u "https://matrix-staging.eclipse.org" -f /config/slack-registration.yaml

docker run -v $PWD/config/:/config/ matrixdotorg/matrix-appservice-slack


# Matrix features

https://matrix-staging.eclipse.org/_matrix/client/versions


# Development

## reuse

```shell
docker run -v $PWD:/data fsfe/reuse:latest lint
```

# devops


## tanka

```
tk show "environments/chat-matrix/dev"
tk apply "environments/chat-matrix/dev"
```

exporting to file:
```
tk show --dangerous-allow-redirect "environments/chat-matrix/dev" > ./k8s/chat-matrix-dev.yaml
```

## kubectl status

Installation
```
kubectl krew install status
```

```
kubectl status deployment -n chat-matrix-dev
```

## Administration with cli

Install from `https://github.com/JOJ0/synadm`

```
sudo pip3 install synadm
```

Connect with:

To find out your admin user's token in Element-Web: Login as this user - "Click User Avatar" - "All Settings" - "Help & About" - Scroll down - "Advanced" - "Access Token"

```
synadm matrix login @admin_username:yourdomain.org
Password:
```

First interaction: don't forget to open tunnel

```
 sudo ./tunnel.sh staging $HOME
./admin/matrix.sh staging
synadm user list
```

# send system alert notification

To a specific user:

```
synadm notice send '@test:matrix-local.eclipse.org' "<b>plain msg</b>"
```

Regex notice: 

```
synadm notice send --regex '(.*):matrix-local.eclipse.org' "<b>msg for all</b>"
```