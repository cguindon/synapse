(import 'chat-matrix/main.libsonnet') +
(import '.secrets/secrets.jsonnet') +
{
  _config+:: {
    local config = self,
    environment: 'staging',
    synapse+: {
      replicas: 2,
      logconfig+: {
        root+: {
          level: 'INFO',
        },
        loggers+: {
          synapse: {
            level: 'INFO',
          },
        },
      },
      homeserver+: {
        local mxDomain = 'matrix.eclipse.org',
        oidc_providers_idp_icon_id::'55b53e24446e3dc22f8f964718bc192adbee0698',
        database+: {
          args+: {
            host: 'postgres-vm1',
          },
        },
        modules: [
          {
            module: 'synapse.modules.synapse_user_control.UserControlModule',
            config: {
              creators: [
                '@sebastien.heurtematte:' + mxDomain,
                '@fred.gurr:' + mxDomain,
              ],
            },
          },
          {
            module: 'synapse.modules.synapse_prevent_encrypt_room.SynapsePreventEncryptRoom',
            config: {
              allow_encryption_for_users: [
                '@sebastien.heurtematte:' + mxDomain,
                '@fred.gurr:' + mxDomain,
              ],
            },
          },
        ],
      },
    },
    matrixMediaRepo+: {
      mediarepo+: {
        database+: {
          host: 'postgres-vm1',
        },
      },
    },
  },
}
