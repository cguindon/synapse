
local kausal = import 'ksonnet-util/kausal.libsonnet';

local util = import '../util.libsonnet';

(import '../config.libsonnet') +
(import './config-synapse.libsonnet') +
(import './config-logconfig.libsonnet') +
(import './config-signing.libsonnet') +
(import './config-homeserver.libsonnet') +
{  
  local this = self,
  local k = kausal { _config+:: this._config },
  
  local deployment = k.apps.v1.deployment,
  local configMap = k.core.v1.configMap,
  local secret = k.core.v1.secret,  
  local config = $._config.synapse,
  local secrets = $._secret.synapse,

  local labels = util.withLabels($._config, config.name),
  local namespace = $._config.namespace,

  synapse: {

    logconfig: util.configMap(config.name + '-log', namespace, labels, 
      {[util.getDomain($._config.matrixDomain, $._config.environment) + '.log.config.yaml']: 
        std.manifestYamlDoc(config.logconfig, indent_array_in_object=true, quote_keys=false)}
    ),

    signing: util.secretData(config.name + '-keys', namespace, labels, 
      {[util.getDomain($._config.matrixDomain, $._config.environment) + '.signing.key']: std.base64(secrets.signing)}
    ),

    homeserver: util.secretStringData(config.name + '-homeserver', namespace, labels, 
      {'homeserver.yaml': std.manifestYamlDoc(config.homeserver,indent_array_in_object=true, quote_keys=false)}
    ),

    deployment: util.deployment(config, namespace, labels) +   
      deployment.emptyVolumeMount(config.name + '-data', config.volume.data.path) +
      deployment.configVolumeMount(config.name + '-log', config.volume.log.path) +
      deployment.secretVolumeMount(config.name + '-keys', config.volume.keys.path, volumeMountMixin={readOnly: true}) +
      deployment.secretVolumeMount(config.name + '-homeserver', config.volume.homeserver.path, volumeMountMixin={readOnly: true}),
    
    service: util.service(self.deployment, namespace, labels),

    route: util.route(config, namespace, labels, disable_cookies='false'),
  },
}