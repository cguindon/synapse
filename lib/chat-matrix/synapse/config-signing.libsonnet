{
  local secret = $._secret,
  _secret+:: {
    "signing": "XXXXXXXXXXXXXXXX"
  },
  _config+:: {
    synapse+: {
      signing: secret.signing
    }        
  }
}